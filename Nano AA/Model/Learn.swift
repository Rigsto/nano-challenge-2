//
//  Learn.swift
//  Nano AA
//
//  Created by Auriga Aristo on 17/04/20.
//  Copyright © 2020 Auriga Aristo. All rights reserved.
//

import Foundation

struct Learn: Decodable {
    let name: String
    let description: String
    let requirements: [String]
    let photo: String
}

struct LearnData: Decodable {
    let data: [Learn]
}
