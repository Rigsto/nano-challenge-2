//
//  Challenge.swift
//  Nano AA
//
//  Created by Auriga Aristo on 14/04/20.
//  Copyright © 2020 Auriga Aristo. All rights reserved.
//

import Foundation

struct Challenge : Decodable {
    let name: String
    let type: String
    let team: Bool
    let description: String
    let goals: [String]
    let duration: Int
    let constraints: [String]
    let deliverables: [String]
    let photo: String
}

struct ChallengeData: Decodable {
    let data: [Challenge]
}
