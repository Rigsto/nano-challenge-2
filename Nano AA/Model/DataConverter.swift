//
//  DataConverter.swift
//  Nano AA
//
//  Created by Auriga Aristo on 14/04/20.
//  Copyright © 2020 Auriga Aristo. All rights reserved.
//

import Foundation

struct ChallengeManager {
    func loadChallenges() -> [Challenge]? {
        guard let fileURL = Bundle.main.url(forResource: R.json.challenge, withExtension: "json") else {
            return nil
        }
        
        do {
            let content = try Data(contentsOf: fileURL)
            let datas = try decode(data: content)
            
            return datas
        } catch let error {
            print(error)
            return nil
        }
    }
    
    func decode(data: Data) throws -> [Challenge] {
        let decoder = JSONDecoder()
        var list: [Challenge] = []
        
        do {
            let decodedData = try decoder.decode(ChallengeData.self, from: data)
            
            for challenge in decodedData.data {
                list.append(challenge)
            }
            
            return list
        } catch {
            return []
        }
    }
}

struct ExplorerManager {
    func decode(data: Data) throws -> [Explorer] {
        let decoder = JSONDecoder()
        var list: [Explorer] = []
        
        do {
            let decodedData = try decoder.decode(ExplorerData.self, from: data)
            
            for explorer in decodedData.data {
                list.append(explorer)
            }
            
            return list
        } catch let error {
            print(error)
            return []
        }
    }
    
    func loadMorningExplorers() -> [Explorer]? {
        guard let fileURL = Bundle.main.url(forResource: R.json.morning, withExtension: "json") else {
            return nil
        }
        
        do {
            let content = try Data(contentsOf: fileURL)
            let datas = try decode(data: content)
            
            return datas
        } catch let error {
            print(error)
            return nil
        }
    }
    
    func loadAfternoonExplorers() -> [Explorer]? {
        guard let fileURL = Bundle.main.url(forResource: R.json.afternoon, withExtension: "json") else {
            return nil
        }
        
        do {
            let content = try Data(contentsOf: fileURL)
            let datas = try decode(data: content)
            
            return datas
        } catch let error {
            print(error)
            return nil
        }
    }
    
    func loadSeniorExplorers() -> [Explorer]? {
        guard let fileURL = Bundle.main.url(forResource: R.json.senior, withExtension: "json") else {
            return nil
        }
        
        do {
            let content = try Data(contentsOf: fileURL)
            let datas = try decode(data: content)
            
            return datas
        } catch let error {
            print(error)
            return nil
        }
    }
}

struct LearnManager {
    func decode(data: Data) throws -> [Learn] {
        let decoder = JSONDecoder()
        var list: [Learn] = []
        
        do {
            let decodedData = try decoder.decode(LearnData.self, from: data)
            
            for learn in decodedData.data {
                list.append(learn)
            }
            
            return list
        } catch let error {
            print(error)
            return []
        }
    }
    
    func loadLearningDatas() -> [Learn]? {
        guard let fileURL = Bundle.main.url(forResource: R.json.learn, withExtension: "json") else {
            return nil
        }
        
        do {
            let content = try Data(contentsOf: fileURL)
            let datas = try decode(data: content)
            
            return datas
        } catch let error {
            print(error)
            return nil
        }
    }
}
