//
//  Explorer.swift
//  Nano AA
//
//  Created by Auriga Aristo on 14/04/20.
//  Copyright © 2020 Auriga Aristo. All rights reserved.
//

import Foundation

struct Explorer: Decodable {
    let name: String
    let mentor: Bool
    let shift: String
    let role: String
    let photo: String
    let learn: [String]
    let help: [String]
}

struct ExplorerData: Decodable {
    let data: [Explorer]
}
