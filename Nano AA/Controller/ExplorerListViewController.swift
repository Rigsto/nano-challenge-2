//
//  ExplorerListViewController.swift
//  Nano AA
//
//  Created by Auriga Aristo on 14/04/20.
//  Copyright © 2020 Auriga Aristo. All rights reserved.
//

import UIKit

class ExplorerListViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var explorerList: [[Explorer]] = [[]]
    var nameList = ["Senior Explorers", "Morning Explorers", "Afternoon Explorers"]
    var sectionId = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let manager = ExplorerManager()
        explorerList[0] = manager.loadSeniorExplorers() ?? []
        explorerList.append(manager.loadMorningExplorers() ?? [])
        explorerList.append(manager.loadAfternoonExplorers() ?? [])
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: R.identifier.explorerCellName, bundle: nil), forCellWithReuseIdentifier: R.identifier.explorerCell)
        
        collectionView.reloadData()
        collectionView.collectionViewLayout.invalidateLayout()
        collectionView.layoutSubviews()
    }
}

extension ExplorerListViewController: UICollectionViewDataSource, UICollectionViewDelegate {

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return explorerList.count > 0 ? 3 : 0
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if explorerList.count > 0 {
            return 4
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.identifier.explorerCell, for: indexPath) as! ExplorerCell
        
        if explorerList[0].count > 0 {
            let datas = explorerList[indexPath.section]
            let dataCell = datas[indexPath.item]

            cell.photo.image = UIImage(named: dataCell.photo)
            cell.name.text = dataCell.name
            cell.job.text = dataCell.role
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let sectionHeaderView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: R.identifier.headerView, for: indexPath) as! SectionHeaderView
        
        if explorerList.count > 0 {
            sectionHeaderView.categoryTitle = nameList[indexPath.section]
            sectionHeaderView.id = indexPath.section
            sectionHeaderView.delegate = self
        }
        
        return sectionHeaderView
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: R.identifier.explorerListSegue, sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if let destination = segue.destination as? ExplorerDetailViewController {
            let indexPath = collectionView.indexPathsForSelectedItems![0]
            destination.explorer = explorerList[indexPath[0]][indexPath[1]]
            destination.similar = getSimilarExplorer(type: indexPath[0], index: indexPath[1])
            
        } else if let destination = segue.destination as? ExplorerShowAllViewController {
            destination.explorers = explorerList[sectionId]
        }
    }
    
    func getSimilarExplorer(type: Int, index: Int) -> [Explorer] {
        let list = explorerList[type]
        var newList = [Explorer]()
        
        for _ in 0...1 {
            var ok = false
            var random = 0
            while !ok {
                random = Int.random(in: 0..<list.count-2)
                if random != index {
                    ok = true
                }
            }
            newList.append(list[random])
        }
        
        return newList
    }
}

extension ExplorerListViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let bounds = collectionView.bounds
        let height = self.view.frame.height
        let width = self.view.frame.width
        let cellSize = (height < width) ? bounds.height / 2 : bounds.width / 2
        return CGSize(width: cellSize - 10, height: cellSize + 10)
    }
}

extension ExplorerListViewController: HeaderViewDelegate {
    func showAllClicked(id: Int) {
        sectionId = id
        performSegue(withIdentifier: R.identifier.showAllSegue, sender: self)
    }
}
