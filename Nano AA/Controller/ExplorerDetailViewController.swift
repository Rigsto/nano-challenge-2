//
//  ExplorerDetailViewController.swift
//  Nano AA
//
//  Created by Auriga Aristo on 16/04/20.
//  Copyright © 2020 Auriga Aristo. All rights reserved.
//

import UIKit

class ExplorerDetailViewController: UIViewController {
    
    @IBOutlet weak var explorerPhoto: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var roleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var canHelpLabel: UILabel!
    @IBOutlet weak var needHelpLabel: UILabel!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var explorer = Explorer(name: "", mentor: false, shift: "", role: "", photo: "", learn: [], help: [])
    var similar: [Explorer] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        explorerPhoto.image = UIImage(named: explorer.photo)
        nameLabel.text = explorer.name
        canHelpLabel.text = convertString(arr: explorer.help)
        needHelpLabel.text = convertString(arr: explorer.learn)
        
        explorer.mentor ? loadSenior() : loadJunior()
        
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: R.identifier.explorerCellName, bundle: nil), forCellWithReuseIdentifier: R.identifier.explorerCell)
    }
    
    func loadJunior() {
        roleLabel.text = "Junior Explorer"
        
        var desc = "Hi, my name is \(explorer.name). I'm a junior explorer which attend to Apple Academy in the \(explorer.shift). "
        
        if explorer.role == "Tech" {
            desc += "I'm expert in coding."
        } else if explorer.role == "Design" {
            desc += "I'm expert in design."
        } else {
            desc += "I'm expert at everything."
        }
        
        descriptionLabel.text = desc
    }
    
    func loadSenior() {
        roleLabel.text = "Senior Explorer"
        
        descriptionLabel.text = "Hi, my name is \(explorer.name). I'm a \(explorer.role) in Apple Academy. You can find me anytime."
    }
    
    func convertString(arr: [String]) -> String {
        return arr.map{String($0)}.joined(separator: "\n")
    }
}

extension ExplorerDetailViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.identifier.explorerCell, for: indexPath) as! ExplorerCell
        
        let data = similar[indexPath.item]
        
        cell.photo.image = UIImage(named: data.photo)
        cell.name.text = data.name
        cell.job.text = data.role
        
        return cell
    }
}

extension ExplorerDetailViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let screenSize = UIScreen.main.bounds
        let screenSizeWidth = screenSize.width
        let size: CGFloat = screenSizeWidth / 2
        return CGSize(width: size, height: size)
    }
}
