//
//  ExplorerShowAllViewController.swift
//  Nano AA
//
//  Created by Auriga Aristo on 21/04/20.
//  Copyright © 2020 Auriga Aristo. All rights reserved.
//

import UIKit

class ExplorerShowAllViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var explorers: [Explorer] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: R.identifier.showAllExplorerCellName, bundle: nil), forCellReuseIdentifier: R.identifier.showAllExplorerCell)
        
        tableView.rowHeight = 120
    }
}

extension ExplorerShowAllViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return explorers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let explorer = explorers[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: R.identifier.showAllExplorerCell, for: indexPath) as! ShowAllExplorerCell
        
        cell.explorerName.text = explorer.name
        cell.explorerJob.text = explorer.role
        cell.explorerPhoto.image = UIImage(named: explorer.photo)
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? ExplorerDetailViewController {
            destination.explorer = explorers[(tableView.indexPathForSelectedRow?.row) ?? 0]
            destination.similar = getSimilarExplorers(index: (tableView.indexPathForSelectedRow?.row) ?? 0)
        }
    }
    
    func getSimilarExplorers(index: Int) -> [Explorer] {
        var newList = [Explorer]()
        
        for _ in 0...1 {
            var ok = false
            var random = 0
            while !ok {
                random = Int.random(in: 0..<explorers.count-1)
                if random != index {
                    ok = true
                }
            }
            newList.append(explorers[random])
        }
        
        return newList
    }
}

extension ExplorerShowAllViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: R.identifier.detailSegue, sender: self)
    }
}
