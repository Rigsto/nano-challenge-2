//
//  SplashViewController.swift
//  Nano AA
//
//  Created by Auriga Aristo on 14/04/20.
//  Copyright © 2020 Auriga Aristo. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        perform(#selector(showMain), with: nil, afterDelay: 3)
    }

    @objc func showMain(){
        performSegue(withIdentifier: R.identifier.splashSegue, sender: self)
    }
}
