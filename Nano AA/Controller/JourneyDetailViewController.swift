//
//  JourneyDetailViewController.swift
//  Nano AA
//
//  Created by Auriga Aristo on 16/04/20.
//  Copyright © 2020 Auriga Aristo. All rights reserved.
//

import UIKit

class JourneyDetailViewController: UIViewController {
    
    @IBOutlet weak var challengeImage: UIImageView!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var goalsLabel: UILabel!
    @IBOutlet weak var constraintsLabel: UILabel!
    @IBOutlet weak var deliverablesLabel: UILabel!
    
    var challenge: Challenge = Challenge(name: "", type: "", team: false, description: "", goals: [], duration: 0, constraints: [], deliverables: [], photo: "")
    
    override func viewDidLoad() {
        super.viewDidLoad()

        challengeImage.image = UIImage(named: challenge.photo)
        typeLabel.text = challenge.team ? "Group Challenge" : "Individual Challenge"
        durationLabel.text = "\(challenge.duration) weeks"
        descriptionLabel.text = "\(challenge.description)"
        goalsLabel.text = convertString(arr: challenge.goals)
        constraintsLabel.text = convertString(arr: challenge.constraints)
        deliverablesLabel.text = convertString(arr: challenge.deliverables)
    }
    
    func convertString(arr: [String]) -> String {
        return arr.map{String($0)}.joined(separator: "\n")
    }
}
