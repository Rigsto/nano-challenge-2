//
//  JourneyListViewController.swift
//  Nano AA
//
//  Created by Auriga Aristo on 14/04/20.
//  Copyright © 2020 Auriga Aristo. All rights reserved.
//

import UIKit

class JourneyListViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var challenges: [Challenge] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: R.identifier.challengeCellName, bundle: nil), forCellReuseIdentifier: R.identifier.challengeCell)
        
        self.tableView.rowHeight = 120
        loadData()
    }
    
    func loadData(){
        let manager = ChallengeManager()
        challenges = manager.loadChallenges() ?? []

        tableView.reloadData()
    }
}

extension JourneyListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return challenges.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let challenge = challenges[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: R.identifier.challengeCell, for: indexPath) as! ChallengeCell
        
        cell.name.text = challenge.name
        cell.type.text = challenge.team ? "Group Challenge" : "Individual Challenge"
        cell.duration.text = "\(challenge.duration) weeks"
        cell.photo.image = UIImage(named: challenge.photo)
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? JourneyDetailViewController {
            destination.challenge = challenges[(tableView.indexPathForSelectedRow?.row) ?? 0]
            destination.title = challenges[tableView.indexPathForSelectedRow?.row ?? 0].name
        }
    }
}

extension JourneyListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: R.identifier.journeyListSegue, sender: self)
    }
}
