//
//  LearnDetailViewController.swift
//  Nano AA
//
//  Created by Auriga Aristo on 17/04/20.
//  Copyright © 2020 Auriga Aristo. All rights reserved.
//

import UIKit

class LearnDetailViewController: UIViewController {
    
    @IBOutlet weak var learnPhoto: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var requirementsLabel: UILabel!
    
    var learn: Learn = Learn(name: "", description: "", requirements: [], photo: "")
    
    override func viewDidLoad() {
        super.viewDidLoad()

        learnPhoto.image = UIImage(named: learn.photo)
        descriptionLabel.text = learn.description
        requirementsLabel.text = convertString(arr: learn.requirements)
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: nil, action: nil)
    }
    
    func convertString(arr: [String]) -> String {
        return arr.map{String($0)}.joined(separator: "\n")
    }
}
