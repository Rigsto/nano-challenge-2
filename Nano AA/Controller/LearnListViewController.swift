//
//  LearnListViewController.swift
//  Nano AA
//
//  Created by Auriga Aristo on 17/04/20.
//  Copyright © 2020 Auriga Aristo. All rights reserved.
//

import UIKit

class LearnListViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var learns: [Learn] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: R.identifier.learnCellName, bundle: nil), forCellReuseIdentifier: R.identifier.learnCell)
        
        tableView.rowHeight = 200
        loadData()
    }
    
    func loadData() {
        let manager = LearnManager()
        learns = manager.loadLearningDatas() ?? []
        
        tableView.reloadData()
    }
}

extension LearnListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return learns.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let learn = learns[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: R.identifier.learnCell, for: indexPath) as! LearnCell
        
        cell.learnImage.image = UIImage(named: learn.photo)
        cell.learnTitle.text = learn.name
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? LearnDetailViewController {
            destination.learn = learns[(tableView.indexPathForSelectedRow?.row) ?? 0]
            destination.title = learns[tableView.indexPathForSelectedRow?.row ?? 0].name
        }
    }
}

extension LearnListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: R.identifier.learnListSegue, sender: self)
    }
}
