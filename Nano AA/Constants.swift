//
//  Constants.swift
//  Nano AA
//
//  Created by Auriga Aristo on 14/04/20.
//  Copyright © 2020 Auriga Aristo. All rights reserved.
//

import Foundation

struct R {
    
    struct identifier {
        //MARK: - segue
        static let splashSegue = "splashToMainSegue"
        static let journeyListSegue = "JourneyListToDetailSegue"
        static let explorerListSegue = "explorerListToDetailSegue"
        static let learnListSegue = "LearnListToDetailSegue"
        static let showAllSegue = "ExplorerListToShowAllSegue"
        static let detailSegue = "ExplorerShowAllToDetailSegue"
        
        //MARK: - XIB Cell
        static let learnCell = "ReusableLearnCell"
        static let learnCellName = "LearnCell"
        static let challengeCell = "ReusableChallengeCell"
        static let challengeCellName = "ChallengeCell"
        static let showAllExplorerCell = "ReusableShowAllExplorerCell"
        static let showAllExplorerCellName = "ShowAllExplorerCell"
        
        //MARK: - collectionView
        static let explorerCell = "ReusableExplorerCell"
        static let explorerCellName = "ExplorerCell"
        static let headerView = "SectionHeaderView"
    }
    
    //MARK: -
    struct json {
        static let challenge = "journey"
        static let morning = "morning"
        static let senior = "senior"
        static let afternoon = "afternoon"
        static let learn = "learn"
        
        static let failed = "Couldn't find selected file"
    }
}
