//
//  SectionHeaderView.swift
//  Nano AA
//
//  Created by Auriga Aristo on 14/04/20.
//  Copyright © 2020 Auriga Aristo. All rights reserved.
//

import UIKit

class SectionHeaderView: UICollectionReusableView {
    
    @IBOutlet weak var categoryTitleLabel: UILabel!
    @IBOutlet weak var seeAll: UIButton!
    
    var id = 0
    var delegate: HeaderViewDelegate!
    
    var categoryTitle: String!{
        didSet {
            categoryTitleLabel.text = categoryTitle
        }
    }
    
    @IBAction func seeAllClicked(_ sender: UIButton) {
        delegate.showAllClicked(id: id)
    }
    
    func setDelegate(delegate: HeaderViewDelegate){
        self.delegate = delegate
    }
}

public protocol HeaderViewDelegate {
    func showAllClicked(id: Int)
}
