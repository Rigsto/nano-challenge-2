//
//  LearnCell.swift
//  Nano AA
//
//  Created by Auriga Aristo on 17/04/20.
//  Copyright © 2020 Auriga Aristo. All rights reserved.
//

import UIKit

class LearnCell: UITableViewCell {
    
    @IBOutlet weak var learnImage: UIImageView!
    @IBOutlet weak var learnTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
