//
//  ExplorerCell.swift
//  Nano AA
//
//  Created by Auriga Aristo on 22/04/20.
//  Copyright © 2020 Auriga Aristo. All rights reserved.
//

import UIKit

class ExplorerCell: UICollectionViewCell {

    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var job: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
