//
//  ShowAllExplorerCell.swift
//  Nano AA
//
//  Created by Auriga Aristo on 17/04/20.
//  Copyright © 2020 Auriga Aristo. All rights reserved.
//

import UIKit

class ShowAllExplorerCell: UITableViewCell {
    
    @IBOutlet weak var explorerPhoto: UIImageView!
    @IBOutlet weak var explorerName: UILabel!
    @IBOutlet weak var explorerJob: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
